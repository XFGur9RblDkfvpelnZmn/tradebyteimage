﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WE_Image
{
    public class Image
    {
        private string colourCodeField;
        private System.DateTime dateModifiedField;
        private string styleNumberField;
        private string fileNameField;

        public System.DateTime DateModified
        {
            get { return dateModifiedField; }
        }
        public string StyleNumber
        {
            get { return styleNumberField; }
        }
        public string ColourCode
        {
            get { return colourCodeField; }
        }
        public string FileName
        {
            get { return fileNameField; }
        } 

        /// <summary>
        /// Get all images (jp*g) in imagepath defined in properties
        /// </summary>
        /// <returns></returns>
        public static List<Image> GetImages()
        {
            List<Image> liReturn = new List<Image>();
            foreach(string sImage in Directory.GetFiles(Properties.Settings.Default.ImagePath,"*.jp*g"))
            {
                FileInfo fi = new FileInfo(sImage);
                string sFileName = fi.Name;
                string sName = Path.GetFileNameWithoutExtension(sImage);
                string[] sData = sName.Split('_');
                if (sData.Length == 3)
                {
                    Image i = new Image();
                    i.fileNameField = sFileName;
                    i.styleNumberField = sData[0];
                    i.colourCodeField = sData[1];
                    i.dateModifiedField = fi.LastWriteTime;
                    liReturn.Add(i);
                }

            }
            return liReturn;
        }

        /// <summary>
        /// Get all images (jp*g) in subfolder "sizeCat" in imagepath defined in properties
        /// </summary>
        /// <param name="sizeCat">subfolder of root imagepath</param>
        /// <returns></returns>
        public static List<Image> GetImages(string sizeCat)
        {
            if(sizeCat.Contains(@"..\") || sizeCat.Contains(@"../"))
            {
                throw new AccessViolationException("Directory traversal is not allowed!");
            }
            List<Image> liReturn = new List<Image>();
            foreach (string sImage in Directory.GetFiles(Properties.Settings.Default.ImagePath+@"\"+sizeCat, "*.jp*g"))
            {
                FileInfo fi = new FileInfo(sImage);
                string sFileName = fi.Name;
                string sName = Path.GetFileNameWithoutExtension(sImage);
                string[] sData = sName.Split('_');
                if (sData.Length == 3)
                {
                    Image i = new Image();
                    i.fileNameField = sFileName;
                    i.styleNumberField = sData[0];
                    i.colourCodeField = sData[1];
                    i.dateModifiedField = fi.LastWriteTime;
                    liReturn.Add(i);
                }

            }
            return liReturn;
        }
    }
}
