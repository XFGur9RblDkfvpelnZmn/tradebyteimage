﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WE_Image
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IImageService
    {
        [OperationContract]
        RetrieveImageLinksResponse RetrieveImages(RetrieveImageLinks ril);

        [OperationContract]
        List<string> RetrieveSizeCats();

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations
    [DataContract]
    public class RetrieveImageLinks 
    {
        private string colourCodeField;    
        private System.DateTime dateModifiedField;    
        private bool dateModifiedFieldSpecified;    
        private string styleNumberField;
        private string sizeCat;

        [DataMember]
        public string ColourCode {
            get {
                return this.colourCodeField;
            }
            set {
                this.colourCodeField = value;
            }
        }

        [DataMember]
        public System.DateTime DateModified {
            get {
                return this.dateModifiedField;
            }
            set {
                this.dateModifiedField = value;
            }
        }

        [DataMember]
        public bool DateModifiedSpecified {
            get {
                return this.dateModifiedFieldSpecified;
            }
            set {
                this.dateModifiedFieldSpecified = value;
            }
        }
        [DataMember]
        public string StyleNumber {
            get {
                return this.styleNumberField;
            }
            set {
                this.styleNumberField = value;
            }
        }
        [DataMember]
        public string SizeCat
        {
            get
            {
                return this.sizeCat;
            }
            set
            {
                this.sizeCat = value;
            }
        }
    }

    [DataContract]
    public class RetrieveImageLinksResponse
    {

        private RetrieveImageLinksResponseImageLinksForIntegrator[] imageLinksForIntegratorField;

        [DataMember]
        public RetrieveImageLinksResponseImageLinksForIntegrator[] ImageLinksForIntegrator
        {
            get
            {
                return this.imageLinksForIntegratorField;
            }
            set
            {
                this.imageLinksForIntegratorField = value;
            }
        }
    }

    [DataContract]
    public class RetrieveImageLinksResponseImageLinksForIntegrator
    {

        private string styleNumberField;

        private string colourCodeField;

        private System.DateTime dateModifiedField;

        private RetrieveImageLinksResponseImageLinksForIntegratorImageLink[] imageLink_ImageLinkResponseField;

        [DataMember]
        public string StyleNumber
        {
            get
            {
                return this.styleNumberField;
            }
            set
            {
                this.styleNumberField = value;
            }
        }

       [DataMember]
        public string ColourCode
        {
            get
            {
                return this.colourCodeField;
            }
            set
            {
                this.colourCodeField = value;
            }
        }

        [DataMember]
        public System.DateTime DateModified
        {
            get
            {
                return this.dateModifiedField;
            }
            set
            {
                this.dateModifiedField = value;
            }
        }

        [DataMember]
        public RetrieveImageLinksResponseImageLinksForIntegratorImageLink[] ImageLink_ImageLinkResponse
        {
            get
            {
                return this.imageLink_ImageLinkResponseField;
            }
            set
            {
                this.imageLink_ImageLinkResponseField = value;
            }
        }
    }
    [DataContract]
    public class RetrieveImageLinksResponseImageLinksForIntegratorImageLink
    {

        private string imageLinkField;

        [DataMember]
        public string ImageLink
        {
            get
            {
                return this.imageLinkField;
            }
            set
            {
                this.imageLinkField = value;
            }
        }
    }
}
