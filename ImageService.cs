﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace WE_Image
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ImageService : IImageService
    {

        public RetrieveImageLinksResponse RetrieveImages(RetrieveImageLinks Request)
        {
            List<Image> li = Request.SizeCat == null || Request.SizeCat == "" ? Image.GetImages() : Image.GetImages(Request.SizeCat);
            List<Image> liReturn = new List<Image>();
            //If no datemodified is provided, select minvalue
            if (Request.DateModified == null)
            {
                Request.DateModified = DateTime.MinValue;
            }
            //CHECK STYLE
            if (Request.StyleNumber != null && Request.StyleNumber.Trim() != string.Empty && (Request.ColourCode == null || Request.ColourCode.Trim() == string.Empty))
            {
                liReturn = li.FindAll(
                    delegate(Image i)
                    {
                        return i.StyleNumber == Request.StyleNumber && i.DateModified > Request.DateModified;
                    }
                );
            }
            //CHECK STYLE + COLOUR
            else if (Request.StyleNumber != null && Request.StyleNumber.Trim() != string.Empty && Request.ColourCode != null && Request.ColourCode.Trim() != string.Empty)
            {
                liReturn = li.FindAll(
                    delegate(Image i)
                    {
                        return i.StyleNumber == Request.StyleNumber && i.ColourCode == Request.ColourCode && i.DateModified > Request.DateModified;
                    }
                );
            }
            //CHECK COLOUR
            else if (Request.ColourCode != null && Request.ColourCode.Trim() != string.Empty)
            {
                throw new NullReferenceException("If ColourCode is provided, the style code is mandatory");
            }
            //MODIFIEDDATE
            else
            {
                liReturn = li.FindAll(
                    delegate(Image i)
                    {
                        return i.DateModified > Request.DateModified;
                    }
                );
            }

            //populate Response (in rilr)
            RetrieveImageLinksResponse rilr = new RetrieveImageLinksResponse();
            int iCount = liReturn.Count;
            RetrieveImageLinksResponseImageLinksForIntegrator[] ilfi = new RetrieveImageLinksResponseImageLinksForIntegrator[iCount];
            for (int i = 0; i < iCount; i++)
            {
                RetrieveImageLinksResponseImageLinksForIntegrator ilfiSub = new RetrieveImageLinksResponseImageLinksForIntegrator();
                ilfiSub.ColourCode = liReturn[i].ColourCode;
                ilfiSub.DateModified = liReturn[i].DateModified;
                ilfiSub.StyleNumber = liReturn[i].StyleNumber;
                ilfi[i] = ilfiSub;
                RetrieveImageLinksResponseImageLinksForIntegratorImageLink[] rilrilfiil = new RetrieveImageLinksResponseImageLinksForIntegratorImageLink[1];
                RetrieveImageLinksResponseImageLinksForIntegratorImageLink rilrilfiilSub = new RetrieveImageLinksResponseImageLinksForIntegratorImageLink();
                rilrilfiilSub.ImageLink = Properties.Settings.Default.URL+liReturn[i].FileName;
                rilrilfiil[0] = rilrilfiilSub;
                ilfiSub.ImageLink_ImageLinkResponse = rilrilfiil;
                rilr.ImageLinksForIntegrator = ilfi;
            }
            

            return rilr;
        }

        public List<string> RetrieveSizeCats()
        {
            List<string> lsReturn = new List<string>();
            DirectoryInfo root = new DirectoryInfo(Properties.Settings.Default.ImagePath);
            DirectoryInfo[] subDirs = root.GetDirectories();

            foreach(DirectoryInfo di in subDirs)
            {
                lsReturn.Add(di.Name);
            }
            return lsReturn;
        }
    }
}
